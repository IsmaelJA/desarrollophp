<?php

echo "CONDICIONALES";

//IF
echo '<br><br>IF<br>';

$edad = 10 ;

if ($edad < 18 ) {
  echo "No eres mayor de edad<br>";
}


//IF ELSE
echo '<br><br>IF ... ELSE<br>';

$edad2 = 25;

if ($edad2 < 18 ) {
  echo "No eres mayor de edad";
}
else {
	echo "Eres mayor de edad";
}

// IF ELSEIF ELSE
echo '<br><br>IF ... ELSEIF ... ELSE<br>';

$edad3 = 20;

if ($edad3 < 18 ) {
  echo "No eres mayor de edad";
}
elseif($edad3 == 20){
	echo "tienes 20 años";
}
else {
	echo "Eres mayor de edad";
}

//SWITCH
echo '<br><br>SWITCH<br>';

$favcolor = "negro";

switch ($favcolor) {
  case "verde":
    echo "Elegiste verde!";
    break;
  case "negro":
    echo "Elegiste negro!";
    break;
  case "azul":
    echo "Elegiste azul!";
    break;
  default:
    echo "No registrado!";
}
