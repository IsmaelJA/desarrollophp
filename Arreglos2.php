<?php

// Podemos definir en un arreglo, clave-valor
$articles = [
    "primero"  => "primer registro",
    "segundo" => "segundo registro",
    "tercero"  => "tercer registro",
    "cuarto" => "cuarto registro"
];

echo "<br> Se imprime todo el arreglo: <br>";
var_dump($articles);
echo "<br>";

echo "<br> Se imprime el SEGUNDO registro del arreglo: <br>";
var_dump($articles["segundo"]);
echo "<br>";

echo "<br> Se imprime el CUARTO registro del arreglo: <br>";
var_dump($articles["cuarto"]);
echo "<br>";
