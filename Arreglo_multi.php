<?php
echo "<b>Arreglos Multidimencionales </b> <br>";
//Arreglos mutidimensionales
$values = [
    "message" => "Hello world!",
    "count"   => 150,
    "pi"      => 3.14,
    "status"  => false,
    "result"  => null
];


$count = 3;
$price = 9.99;

// arreglos con variables
$data = [$count, $price];
echo "<br> Se imprime un arreglo de variables: <br>";
var_dump($data);
echo "<br>";


// Arreglo bidiensionale
$articles = [
    ["title" => "First post",   "content" => "This is the first post"],
    ["title" => "Another post", "content" => "Another post to read here"],
    ["title" => "Read this!",   "content" => "You must read this article!"]
];

echo "<br> Se imprime un arreglo multidimensionael: <br>";
var_dump($articles);
echo "<br>";

echo "<br>El arreglo articles tiene: " .count($articles) ." elementos" ;
echo "<br>";

echo "<br> Se imprime registro 1 del arreglo multidimensionael: <br>";
var_dump($articles[1]["title"]);
echo "<br>";

//Segundo arreglo multidimensional
$carros2 = array (
  array("Volvo",22,18),
  array("BMW",15,13),
  array("Saab",5,2),
  array("Land Rover",17,15)
);

echo "<br> Se imprimen registros de otro arreglo multidimensional arreglo multidimensionael: <br>";
//accdemos a cada elemento de los arreglos mediante sus indices
// Primer corchete especifica las posiciones primer arreglo y el segundo corchete las posiciones de 2do arregl
echo $carros2[0][0].": stock: ".$carros2[0][1].", vendidos: ".$carros2[0][2].".<br>";
echo $carros2[1][0].": stock: ".$carros2[1][1].", vendidos: ".$carros2[1][2].".<br>";
echo $carros2[2][0].": stock: ".$carros2[2][1].", vendidos: ".$carros2[2][2].".<br>";
echo $carros2[3][0].": stock: ".$carros2[3][1].", vendidos: ".$carros2[3][2].".<br>";


//UTILIZANDO LA LONGITUD VAIRABLE DE LOS ARRAY
echo "<br>";
echo "Impresion dinamica de arreglos multidimencionales";
echo '<br>Podemos obtener los limites de la iteracion con la funcion count()<br>';

$carros4 = array (
  array("Volvo",22,18,2020),
  array("BMW",15,13,2019),
  array("Saab",5,2,2018),
  array("Land Rover",17,15,2017)
);

//Cuando colocamos la condicion $array_interno < 4 del ejemplo anterior, lo hicmos con codigo duro, es decir, datos que sabemos que son correctos, pero que en un ambiente productivo, no sabremos, por lo que es mejor manejar la condicion del cilo for mediante la longitud del array, sin importar cual sea, con la funcion  count()
for ($array_interno = 0; $array_interno < count($carros4); $array_interno++) {
  echo "<p><b>Array interno: $array_interno</b></p>";
  echo "<ul>";
  for ($info_array_interno = 0; $info_array_interno < count($carros4[$array_interno]); $info_array_interno++) {
    echo "<li>".$carros4[$array_interno][$info_array_interno]."</li>";
  }
  echo "</ul>";
}
