<?php

// Arreglo con IF

$articulos = ['1'=> "Primera publicacion",
              '2'=> "Segunda publicacion",
              '3'=> "Tercera publicacion" ];

if (empty($articulos)) {
    echo "El arreglo esta vacio";
} else {
    echo "El arreglo no esta vacio";
    echo "<br>";
    echo "Este es el contenido del arreglo: <br>";
    foreach ($articulos as $index => $article) {
        echo "clave: " .$index . ' - ' ."valor: " .$article, ", ";
        echo "<br>";
}
}
