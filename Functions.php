<?php

//NUMBER FUNCTIONS
echo '<br><br>Number functions<br>';

//funcion pi
echo '<br>PI<br>';

echo(pi()); // regresa 3.1415926535898
echo "<br>";

//funcion min and max()
echo '<br><br>MIN Y MAX<br>';
echo(min(0, 150, 30, 20, -8, -200));  // regresa -200
echo "<br>";

echo(max(0, 150, 30, 20, -8, -200));  // regresa 150
echo "<br>";

//funcion valor absoluto
echo '<br><br>Valor absoluto<br>';
echo(abs(-6.7));  // regresa en positivo 6.7
echo "<br>";

//funcion raiz cuadrada
echo '<br><br>Valor absoluto<br>';
echo(sqrt(64));  // regresa 8
echo "<br>";

//funcion redondear
echo '<br><br>Round<br>';
echo(round(0.60));  // regresa 1
echo "<br>";

echo(round(0.49));  // regresa 0
echo "<br>";

//funcion random
echo '<br><br>Random<br>';
echo(rand()); //numero aleatorio totalmente
echo "<br>";

echo(rand(10, 100)); //numero entre 10 y 100 inclusive
echo "<br>";
