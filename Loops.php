<?php
echo '<br><br>LOOPS<br>';

//WHILE
echo '<br><br>WHILE<br>';

$x = 1;

while($x <= 5) {
  echo "The number is: $x <br>";
  $x++;
}

echo '<br>Segundo while , ahora x vale 6<br>';

while($x <= 100) {

  echo "The number is: $x <br>";
  $x+=10;
}

//DO WHILE
echo '<br><br>LOOP DO WHILE<br>';

$y = 1;

do {
  echo "The number is: $y <br>";
  $y++;
} while ($y <= 5);

//FOR
echo '<br><br>FOR<br>';

for ($contador = 0; $contador <= 10; $contador++) {
	echo "El contador vale: $contador <br>";
}

//FOREACH
echo '<br><br>FOR EACH<br>';

$colores = array("rojo", "verde", "azul", "amarillo");

foreach ($colores as $color) {
  echo "$color <br>";
}

?>
