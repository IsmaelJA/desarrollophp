//CAST FLOAT AND STRING TO INT
echo '<br><br>CAST FLOAT AND STRING TO INT<br>';

//Cast float to int
$float_to_cast = 23465.768;
$float_to_cast = (int)$float_to_cast;
echo $float_to_cast;

echo "<br>";

// Cast string to int
$string_to_cast = "23465.768";
$int_cast2 = (int)$string_to_cast;
echo $int_cast2;
